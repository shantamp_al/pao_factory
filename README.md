# Pao Factory TDD project 🍞

This project is to explore the coding of a bread factory. 

We will cover topics including:
- Git
- Bitbucket
- Markdown
- Agile/Scrum
- TDD (Test Driven Development)
- Unit tests
- Functional programming

Other principles:
- Seperation of Concerns
- DRY code

### TDD amd Unit Tests

Unit test is a single test that test a part of a function. Several tests together will help ensure the functionallity of the programme, reduce technical debt and keep it maintaintable.

Generally code lives, entropy adds complexity and short cuts lead to technical debt that can kill a project.

Technical debt is the concept of taking shortcuts - like skipping documentation or not making unit tets - leading to unmanagable code.
Other factors such as tie, people leaving, updates also add to technical debt.

** Test Driven Development** Is a way of developing code that is  very Agile.
:) It's the lightest implementation of Agile - Ensures working code.

You make the first, then you code the least amount of code to make test pass.

### User Stories and tests

Good user-stories can be used to make unit tests. These are usually done by the `Three Amigos` `Dev` + `Testers` + ` Business Analyist`

In our case it will be just us.

**User Story 1** As a bread maker, I want to provide to `flour` and `water` and get out `dough`, else I want `not dough`, So that I can then bake the bread.

**User Story 2** As a bread maker, I want to be able take the `dough` and use the `baker` to get `Pao`. Anything else I should get `not Pao`. So that I can make bread.

**User Story 3** As a bread maker, I want an option of `run_factory` that will take in `flour` and `water` and give me `pao`, else give me `not Pao`. So i can make bread with one simple action.





