# Define functions (not for running and calling)

def make_dough_option(arg1, arg2):
    if 'flour' == arg1 and 'water' == arg2:
        return 'dough'
    elif 'water' == arg1 and 'flour' == arg2:
        return 'dough'
    else:
        return 'not dough'

def bake_dough(arg1, arg2):
    if 'dough' in arg1 and 'baker' in arg2:
        return 'Pão'
    else:
        return 'not Pão'

def run_factory(arg1, arg2):
    if 'flour' in arg1 and 'water' in arg2:
        return 'Pão'
    else:
        return 'not Pão'


## 3 refactered

def run_factory(arg1, arg2):
    if arg1 in ['flour', 'water'] and arg2 in ['flour', 'water'] and arg1 != arg2:
        return 'Pão'
    else:
        return 'not Pão'

### ^^^ English terms - If argument1 is in the list of Flour and Water and argument2 is in the list of Flour and water and both arguments should not be the same, it will return 'Pão'
 