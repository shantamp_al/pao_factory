# # Import functions and run tests

from factory_functions import *

# # What is a test?
# # It's an assertion, with a know outcome of a function that comes back with True or False

# # **User Story 1** As a bread maker, I want to provide to `flour` and `water` and get out `dough`, else I want `not dough`, So that I can then bake the bread.
print("test number 1 - make_dough_option with 'flour' and 'water' should equal 'dough'")
print(make_dough_option('flour','water') == 'dough')

print("test number 2 - make_dough_option with 'cement' and 'water' should equal 'not dough'")
print(make_dough_option('cement', 'water') == 'not dough')

# # user story 2 + make tests + functions
print("test number 3 - bake_dough by taking 'dough' and using 'baker' should equal 'Pão' ")
print(bake_dough('dough', 'baker') == 'Pão')

print("test number 4 - bake_dough with 'dough' and 'oven' should equal 'not Pão'")
print(bake_dough('dough', 'oven') == 'not Pão')

# user story 3 + make tests + functions
print("test number 5 - run_factory by taking 'flour' and 'water' should equal 'Pão'")
print(run_factory('flour', 'water') == 'Pão')

print("test number 6 - run_factory with 'flour' and 'milk' should equal 'not Pão'" )
print(run_factory('flour', 'milk') == 'not Pão')

# user story 3 ( refactored) + make tests + functions
print("test number 5 (refactored) - we are using 'flour' and 'water' to create 'Pão' therefore the statement should return True")
print(run_factory('flour', 'water') == 'Pão' )

print("test number 6 (refactored) - we are using 'flour' and 'milk' which we cannot make 'Pão' and therefore should return True")
print(run_factory('flour', 'milk') == 'not Pão')



