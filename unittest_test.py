import unittest
from factory_functions import *

class TestBakingFunctions(unittest.TestCase):

## Test Step 1
    def test_make_dough_correct(self):
        self.assertEqual( make_dough_option('flour', 'water'), 'dough')

    def test_make_dough_incorrect(self):
        self.assertEqual( make_dough_option('cement', 'water'), 'not dough')

## Test Step 2
    def test_bake_dough_correct(self):
        self.assertEquals( bake_dough('dough', 'baker'), 'Pão' )

    def test_bake_dough_incorrect(self):
        self.assertEqual( bake_dough('dough', 'oven'), 'not Pão')
    
if __name__ == '__main__':
    unittest.main()

 

    